/////////////////////////////////////////////////////////////
//		statusSheet.h
//		This is basically the database of prerequisits
//		and methods to interact with the data base
/////////////////////////////////////////////////////////////

#include<string>
#include<vector>
#include<fstream>
#pragma once
using namespace std;

class statusSheet{
public:
	statusSheet();
	statusSheet(fstream&);
	struct prerecs{
		string code;
		string name;
		vector<string> prerequisites
		string prerequisite;
		bool hasTakenPrerequiste;
		bool canTake;
	};
	struct sheet{
		string code;
		string name;
	};
	vector<prerecs> searchCode(string, vector<prerecs>);
	vector<prerecs> searchPrerequisite(string, vector<prerecs>);
	void markAsTaken(string, vector<prerecs>);
	bool takenAllPrerequisites(string, vector<sheet>);
private:
	vector<prerecs> prerequisites;
	vector<sheet> sSheet;
};
