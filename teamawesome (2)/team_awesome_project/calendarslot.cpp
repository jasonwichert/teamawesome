#include "calendarslot.h"
void CalendarSlot::changeText(QString t)
{
	if(text()=="")//If no conflict
	{
		setText(t);
		//helper method, setToolTip(helpermethod returns qstring)
		setToolTip(getCourseInfo(t));
		conflictingCourses.push_back(std::string(t.toUtf8()));//in case there is a conflict in the future
	}
	else
	{
		conflictingCourses.push_back(std::string(t.toUtf8()));
		setText(QString("CONFLICT!"));
	}
}
void CalendarSlot::deleteCourse(bool)
{
	if(text()==QString("CONFLICT!"))//Show the original class
	{
		QString nextClass = QString(conflictingCourses.at(0).c_str());//Go to the next possible class choice.
		setText(nextClass);
		int x;
	}
	else if(conflictingCourses.size()==1)//If there is no conflict, delete the course.
	{
		QString blank = "";//
		setText(blank);
		conflictingCourses.erase(conflictingCourses.cbegin());//erase the course, the user doesn't want it.
	}
	else if(conflictingCourses.size()>1)//Deal with the conflict
	{
		conflictingCourses.erase(conflictingCourses.cbegin());//erase the course, the user doesn't want it.
		QString nextClass = QString(conflictingCourses.at(0).c_str());//Go to the next possible class choice.
		setText(nextClass);
	}
	//If there are no courses we don't have to do anything
}

QString CalendarSlot::getCourseInfo(QString code)
{
	//make database, search it, find course and get info, concatenate into one string
	/*fstream file;//overall course database
	fstream prereqs;//prerequisites
	file.open("CourseDB_WithFictionalCapacities (1).txt");
	prereqs.open("prerequisites.txt");
	AvalibleClasses* avalible = new AvalibleClasses(file,prereqs);*/
	vector<AvalibleClasses::Classes> results = avalible->searchCode(code.toStdString(),avalible->getList());
	string moreInfo = "Ends at: " + results.at(0).endTime + "; " + "Location: " + results.at(0).building + " " + results.at(0).room;
	return QString(moreInfo.c_str());
}