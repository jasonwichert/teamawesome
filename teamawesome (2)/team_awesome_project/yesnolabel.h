#include <QObject>
#include <QLabel> 
#include "AvalibleClasses.h"
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QApplication>
#pragma once
class YesNoLabel:public QLabel
{
	Q_OBJECT
public:
	YesNoLabel(QWidget* parent = nullptr): QLabel(parent) {yesNo=false;}
public slots:
	void toggle(bool);//Change from x to check or vice versa.
	void check(){yesNo=true;setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/yes.jpg\"/></p></body></html>", 0));};//Change to check
signals:
private:
	bool yesNo;//True for checkmark image, false for x image.
};