#ifndef TEAM_AWESOME_PROJECT_H
#define TEAM_AWESOME_PROJECT_H

#include <QtWidgets/QMainWindow>
#include "ui_team_awesome_project.h"
#include "AvalibleClasses.h"
class team_awesome_project : public QMainWindow
{
	Q_OBJECT

public:
	team_awesome_project(QWidget *parent = 0);
	~team_awesome_project();
	AvalibleClasses* getAvalible() {return avalible;}
signals:
	void declareAvailableClasses(AvalibleClasses);//lets every object that need to know the available classes do so.
private:
	Ui::team_awesome_projectClass ui;
	AvalibleClasses* avalible;
};

#endif // TEAM_AWESOME_PROJECT_H
