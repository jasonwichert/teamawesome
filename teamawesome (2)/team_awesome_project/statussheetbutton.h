#include <QObject>
#include <QPushButton> 
#include <string>
#include <vector>
#include "status.h"
#include "AvalibleClasses.h"
#pragma once
class StatusSheetButton:public QPushButton
{
	Q_OBJECT
public:
	StatusSheetButton(QWidget* parent = nullptr): QPushButton(parent) {};
public slots:
	void gotPushed(bool);//What the button does when it got pushed
	void courseAdded(QString code){stat1.announceTaken(code);}//Course was added, we need to check if it is on the status sheet to add it.
signals:
	void sendToGoButton(QString);//Send a course code to the go button to be listed as taken or untaken.
private:
	status stat1;//The status sheet window
};