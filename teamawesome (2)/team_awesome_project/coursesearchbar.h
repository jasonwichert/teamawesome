#include <QObject>
#include <QTextEdit> 
#include <string>
#pragma once
class CourseSearchBar:public QTextEdit
{
	Q_OBJECT
public:
	CourseSearchBar(QWidget* parent = nullptr): QTextEdit(parent) {};
public slots:
	void recieveCourseNameRequest();
signals:
	void sendCourseName(QString);
private:
};
