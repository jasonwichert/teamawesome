#include "coursesearchbar.h"

void CourseSearchBar::recieveCourseNameRequest()
{
	QString courseName(toPlainText());
	emit sendCourseName(courseName);
}