#include <QObject>
#include <QPushButton> 
#include <string>
#include <vector>
#include "AvalibleClasses.h"
#pragma once
class GoButton:public QPushButton
{
	Q_OBJECT
public:
	GoButton(QWidget* parent = nullptr): QPushButton(parent) {courseName="";dateTime="";};
public slots:
	void courseChanged();
	void recieveCourseName(QString name);
	void dateTimeChanged(QString time);
	void gotPushed(bool);
	void courseAdded(int);
	void markAsTaken(QString);
signals:
	void requestCourseName();
	void changeResult1(QString);
	void changeResult2(QString);
	void changeResult3(QString);
	void changeResult4(QString);
	void changeResult5(QString);
	void changeResult6(QString);
	void changeResult7(QString);
	void changeResult8(QString);
	void changeResult9(QString);
	void changeResult10(QString);
	void changeResult11(QString);
	void changeResult12(QString);
	void changeResult13(QString);
	void changeResult14(QString);
	void changeResult15(QString);
	void changeResult16(QString);
	void changeResult17(QString);
	void changeResult18(QString);
	void changeResult19(QString);
	void changeResult20(QString);
	void changeResult21(QString);
	void changeResult22(QString);
	void changeResult23(QString);
	void changeResult24(QString);
	void changeResult25(QString);
	void addClass8mwf(QString);
	void addClass9mwf(QString);
	void addClass10mwf(QString);
	void addClass11mwf(QString);
	void addClass12mwf(QString);
	void addClass1mwf(QString);
	void addClass2mwf(QString);
	void addClass3mwf(QString);
	void addClass8tr(QString);
	void addClass10tr(QString);
	void addClass11tr(QString);
	void addClass1tr(QString);
	void addClass2tr(QString);
	void addClass6m(QString);
	void addClass6t(QString);
	void addClass6w(QString);
	void addClass6r(QString);
	void addClassInd(QString);
private:
	//AvalibleClasses* available;
	std::string courseName;
	std::string dateTime;
	std::vector<AvalibleClasses::Classes> searchResults;
};

