/////////////////////////////////////////////////////////////
//		AvalibleClasses.h
//		This is basically the database of courses
//		and methods to interact with the data base
/////////////////////////////////////////////////////////////

#include<string>
#include<vector>
#include<fstream>
#include<qmetatype.h>
#pragma once
using namespace std;

class AvalibleClasses{
public:
	AvalibleClasses();
	//Takes the given file of classes and parses it into classes
	AvalibleClasses(fstream&, fstream&);
	struct Classes{
		//Searchable information
		string code;
		string name;
		string time;
		string day;

		//Other Info
		string shortTitle;
		string endTime;
		string building;
		string room;
		int enrollment;
		int capacity;
		vector<string> prerequisite;
		//used in the status sheet
		bool hasTaken;
		bool canTake;
	};
	vector<Classes> searchCode(string, vector<Classes>);
	vector<Classes> searchName(string, vector<Classes>);
	vector<Classes> searchTime(string, vector<Classes>);
	vector<Classes> searchDay(string, vector<Classes>);
	vector<Classes> courseSearch(string courseName, string dateTime);
	//Takes user input and coverts it into a useable string
		//Makes everything capitalized
		//Replaces " " with "_"
	string convert(string);
	//parses DateTime into time and days
	vector<string> parseDateTime(string);
	//combines two vectors returning only the unique elements
	vector<Classes> merge(vector<Classes>, vector<Classes>);
	//returns true if prerequisite is met
	bool checkCanTake(string);
	void markAsTaken(string);
	void unMarkAsTaken(string);
	void buildStatusSheet(fstream&);
	vector<Classes> getList() {return list;}
private:
	//A vector of all the classes in the database
	vector<Classes> list;
	vector<vector<string>> statusSheetClasses;
};
Q_DECLARE_METATYPE(AvalibleClasses);//This is so we can pass AvalibleClasses object through signal
