#include "team_awesome_project.h"
#include "status.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{

	qRegisterMetaType<AvalibleClasses>("AvalibleClasses");//This is so we can pass AvalibleClasses object through signal
	QApplication a(argc, argv);
	team_awesome_project w;
	w.declareAvailableClasses(*(w.getAvalible()));//Tell the other objects what classes are available.
	w.show();
	return a.exec();
}
