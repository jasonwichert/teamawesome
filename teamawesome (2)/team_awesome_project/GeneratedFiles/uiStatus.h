/********************************************************************************
** Form generated from reading UI file 'status.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STATUS_H
#define UI_STATUS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include "takenbutton.h"
#include "yesnolabel.h"

QT_BEGIN_NAMESPACE

class ui_statusClass
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_2;
    YesNoLabel *label_15;
    TakenButton *pushButton_7;
    TakenButton *pushButton_24;
    TakenButton *pushButton_21;
    TakenButton *pushButton_47;
    TakenButton *pushButton_26;
    TakenButton *pushButton_19;
    YesNoLabel *label_17;
    YesNoLabel *label_33;
    YesNoLabel *label_39;
    TakenButton *pushButton_12;
    TakenButton *pushButton_13;
    TakenButton *pushButton_31;
    TakenButton *pushButton_20;
    YesNoLabel *label_21;
    TakenButton *pushButton_16;
    YesNoLabel *label_22;
    TakenButton *pushButton_22;
    YesNoLabel *label_25;
    YesNoLabel *label_23;
    TakenButton *pushButton_15;
    TakenButton *pushButton_23;
    YesNoLabel *label_6;
    TakenButton *pushButton_29;
    TakenButton *pushButton_5;
    TakenButton *pushButton_14;
    YesNoLabel *label_7;
    YesNoLabel *label_18;
    YesNoLabel *label_13;
    YesNoLabel *label_5;
    YesNoLabel *label_10;
    TakenButton *pushButton_6;
    YesNoLabel *label;
    TakenButton *pushButton_11;
    YesNoLabel *label_20;
    YesNoLabel *label_24;
    TakenButton *pushButton_17;
    TakenButton *pushButton_18;
    TakenButton *pushButton_4;
    TakenButton *pushButton_10;
    TakenButton *pushButton_9;
    YesNoLabel *label_11;
    TakenButton *pushButton_41;
    YesNoLabel *label_47;
    YesNoLabel *label_30;
    YesNoLabel *label_38;
    YesNoLabel *label_14;
    TakenButton *pushButton_42;
    TakenButton *pushButton_39;
    TakenButton *pushButton_3;
    YesNoLabel *label_31;
    YesNoLabel *label_48;
    YesNoLabel *label_8;
    YesNoLabel *label_9;
    YesNoLabel *label_45;
    TakenButton *pushButton_32;
    TakenButton *pushButton_8;
    YesNoLabel *label_29;
    TakenButton *pushButton_43;
    YesNoLabel *label_3;
    YesNoLabel *label_19;
    YesNoLabel *label_4;
    YesNoLabel *label_16;
    TakenButton *pushButton_2;
    YesNoLabel *label_12;
    TakenButton *pushButton;
    QLabel *label_2;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(861, 838);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        scrollArea = new QScrollArea(centralwidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 824, 813));
        gridLayout_2 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_15 = new YesNoLabel(scrollAreaWidgetContents);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_2->addWidget(label_15, 6, 3, 1, 1);

        pushButton_7 = new TakenButton(scrollAreaWidgetContents);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));

        gridLayout_2->addWidget(pushButton_7, 6, 0, 1, 1);

        pushButton_24 = new TakenButton(scrollAreaWidgetContents);
        pushButton_24->setObjectName(QStringLiteral("pushButton_24"));

        gridLayout_2->addWidget(pushButton_24, 6, 4, 1, 1);

        pushButton_21 = new TakenButton(scrollAreaWidgetContents);
        pushButton_21->setObjectName(QStringLiteral("pushButton_21"));

        gridLayout_2->addWidget(pushButton_21, 8, 4, 1, 1);

        pushButton_47 = new TakenButton(scrollAreaWidgetContents);
        pushButton_47->setObjectName(QStringLiteral("pushButton_47"));

        gridLayout_2->addWidget(pushButton_47, 10, 2, 1, 1);

        pushButton_26 = new TakenButton(scrollAreaWidgetContents);
        pushButton_26->setObjectName(QStringLiteral("pushButton_26"));

        gridLayout_2->addWidget(pushButton_26, 9, 2, 1, 1);

        pushButton_19 = new TakenButton(scrollAreaWidgetContents);
        pushButton_19->setObjectName(QStringLiteral("pushButton_19"));

        gridLayout_2->addWidget(pushButton_19, 1, 4, 1, 1);

        label_17 = new YesNoLabel(scrollAreaWidgetContents);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_2->addWidget(label_17, 8, 3, 1, 1);

        label_33 = new YesNoLabel(scrollAreaWidgetContents);
        label_33->setObjectName(QStringLiteral("label_33"));

        gridLayout_2->addWidget(label_33, 10, 3, 1, 1);

        label_39 = new YesNoLabel(scrollAreaWidgetContents);
        label_39->setObjectName(QStringLiteral("label_39"));

        gridLayout_2->addWidget(label_39, 10, 1, 1, 1);

        pushButton_12 = new TakenButton(scrollAreaWidgetContents);
        pushButton_12->setObjectName(QStringLiteral("pushButton_12"));

        gridLayout_2->addWidget(pushButton_12, 7, 2, 1, 1);

        pushButton_13 = new TakenButton(scrollAreaWidgetContents);
        pushButton_13->setObjectName(QStringLiteral("pushButton_13"));

        gridLayout_2->addWidget(pushButton_13, 8, 2, 1, 1);

        pushButton_31 = new TakenButton(scrollAreaWidgetContents);
        pushButton_31->setObjectName(QStringLiteral("pushButton_31"));

        gridLayout_2->addWidget(pushButton_31, 9, 4, 1, 1);

        pushButton_20 = new TakenButton(scrollAreaWidgetContents);
        pushButton_20->setObjectName(QStringLiteral("pushButton_20"));

        gridLayout_2->addWidget(pushButton_20, 7, 4, 1, 1);

        label_21 = new YesNoLabel(scrollAreaWidgetContents);
        label_21->setObjectName(QStringLiteral("label_21"));

        gridLayout_2->addWidget(label_21, 4, 5, 1, 1);

        pushButton_16 = new TakenButton(scrollAreaWidgetContents);
        pushButton_16->setObjectName(QStringLiteral("pushButton_16"));

        gridLayout_2->addWidget(pushButton_16, 6, 2, 1, 1);

        label_22 = new YesNoLabel(scrollAreaWidgetContents);
        label_22->setObjectName(QStringLiteral("label_22"));

        gridLayout_2->addWidget(label_22, 7, 5, 1, 1);

        pushButton_22 = new TakenButton(scrollAreaWidgetContents);
        pushButton_22->setObjectName(QStringLiteral("pushButton_22"));

        gridLayout_2->addWidget(pushButton_22, 4, 4, 1, 1);

        label_25 = new YesNoLabel(scrollAreaWidgetContents);
        label_25->setObjectName(QStringLiteral("label_25"));

        gridLayout_2->addWidget(label_25, 8, 5, 1, 1);

        label_23 = new YesNoLabel(scrollAreaWidgetContents);
        label_23->setObjectName(QStringLiteral("label_23"));

        gridLayout_2->addWidget(label_23, 6, 5, 1, 1);

        pushButton_15 = new TakenButton(scrollAreaWidgetContents);
        pushButton_15->setObjectName(QStringLiteral("pushButton_15"));

        gridLayout_2->addWidget(pushButton_15, 5, 2, 1, 1);

        pushButton_23 = new TakenButton(scrollAreaWidgetContents);
        pushButton_23->setObjectName(QStringLiteral("pushButton_23"));

        gridLayout_2->addWidget(pushButton_23, 5, 4, 1, 1);

        label_6 = new YesNoLabel(scrollAreaWidgetContents);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_2->addWidget(label_6, 5, 1, 1, 1);

        pushButton_29 = new TakenButton(scrollAreaWidgetContents);
        pushButton_29->setObjectName(QStringLiteral("pushButton_29"));

        gridLayout_2->addWidget(pushButton_29, 10, 0, 1, 1);

        pushButton_5 = new TakenButton(scrollAreaWidgetContents);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        gridLayout_2->addWidget(pushButton_5, 5, 0, 1, 1);

        pushButton_14 = new TakenButton(scrollAreaWidgetContents);
        pushButton_14->setObjectName(QStringLiteral("pushButton_14"));

        gridLayout_2->addWidget(pushButton_14, 4, 2, 1, 1);

        label_7 = new YesNoLabel(scrollAreaWidgetContents);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_2->addWidget(label_7, 5, 3, 1, 1);

        label_18 = new YesNoLabel(scrollAreaWidgetContents);
        label_18->setObjectName(QStringLiteral("label_18"));

        gridLayout_2->addWidget(label_18, 5, 5, 1, 1);

        label_13 = new YesNoLabel(scrollAreaWidgetContents);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_2->addWidget(label_13, 4, 3, 1, 1);

        label_5 = new YesNoLabel(scrollAreaWidgetContents);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_2->addWidget(label_5, 4, 1, 1, 1);

        label_10 = new YesNoLabel(scrollAreaWidgetContents);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_2->addWidget(label_10, 6, 1, 1, 1);

        pushButton_6 = new TakenButton(scrollAreaWidgetContents);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        gridLayout_2->addWidget(pushButton_6, 7, 0, 1, 1);

        label = new YesNoLabel(scrollAreaWidgetContents);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_2->addWidget(label, 1, 1, 1, 1);

        pushButton_11 = new TakenButton(scrollAreaWidgetContents);
        pushButton_11->setObjectName(QStringLiteral("pushButton_11"));

        gridLayout_2->addWidget(pushButton_11, 1, 2, 1, 1);

        label_20 = new YesNoLabel(scrollAreaWidgetContents);
        label_20->setObjectName(QStringLiteral("label_20"));

        gridLayout_2->addWidget(label_20, 3, 5, 1, 1);

        label_24 = new YesNoLabel(scrollAreaWidgetContents);
        label_24->setObjectName(QStringLiteral("label_24"));

        gridLayout_2->addWidget(label_24, 2, 5, 1, 1);

        pushButton_17 = new TakenButton(scrollAreaWidgetContents);
        pushButton_17->setObjectName(QStringLiteral("pushButton_17"));

        gridLayout_2->addWidget(pushButton_17, 3, 4, 1, 1);

        pushButton_18 = new TakenButton(scrollAreaWidgetContents);
        pushButton_18->setObjectName(QStringLiteral("pushButton_18"));

        gridLayout_2->addWidget(pushButton_18, 2, 4, 1, 1);

        pushButton_4 = new TakenButton(scrollAreaWidgetContents);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        gridLayout_2->addWidget(pushButton_4, 4, 0, 1, 1);

        pushButton_10 = new TakenButton(scrollAreaWidgetContents);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));

        gridLayout_2->addWidget(pushButton_10, 2, 2, 1, 1);

        pushButton_9 = new TakenButton(scrollAreaWidgetContents);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));

        gridLayout_2->addWidget(pushButton_9, 3, 2, 1, 1);

        label_11 = new YesNoLabel(scrollAreaWidgetContents);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_2->addWidget(label_11, 1, 3, 1, 1);

        pushButton_41 = new TakenButton(scrollAreaWidgetContents);
        pushButton_41->setObjectName(QStringLiteral("pushButton_41"));

        gridLayout_2->addWidget(pushButton_41, 10, 4, 1, 1);

        label_47 = new YesNoLabel(scrollAreaWidgetContents);
        label_47->setObjectName(QStringLiteral("label_47"));

        gridLayout_2->addWidget(label_47, 11, 1, 1, 1);

        label_30 = new YesNoLabel(scrollAreaWidgetContents);
        label_30->setObjectName(QStringLiteral("label_30"));

        gridLayout_2->addWidget(label_30, 10, 5, 1, 1);

        label_38 = new YesNoLabel(scrollAreaWidgetContents);
        label_38->setObjectName(QStringLiteral("label_38"));

        gridLayout_2->addWidget(label_38, 9, 3, 1, 1);

        label_14 = new YesNoLabel(scrollAreaWidgetContents);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_2->addWidget(label_14, 7, 3, 1, 1);

        pushButton_42 = new TakenButton(scrollAreaWidgetContents);
        pushButton_42->setObjectName(QStringLiteral("pushButton_42"));

        gridLayout_2->addWidget(pushButton_42, 9, 0, 1, 1);

        pushButton_39 = new TakenButton(scrollAreaWidgetContents);
        pushButton_39->setObjectName(QStringLiteral("pushButton_39"));

        gridLayout_2->addWidget(pushButton_39, 11, 0, 1, 1);

        pushButton_3 = new TakenButton(scrollAreaWidgetContents);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        gridLayout_2->addWidget(pushButton_3, 3, 0, 1, 1);

        label_31 = new YesNoLabel(scrollAreaWidgetContents);
        label_31->setObjectName(QStringLiteral("label_31"));

        gridLayout_2->addWidget(label_31, 9, 1, 1, 1);

        label_48 = new YesNoLabel(scrollAreaWidgetContents);
        label_48->setObjectName(QStringLiteral("label_48"));

        gridLayout_2->addWidget(label_48, 11, 3, 1, 1);

        label_8 = new YesNoLabel(scrollAreaWidgetContents);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_2->addWidget(label_8, 7, 1, 1, 1);

        label_9 = new YesNoLabel(scrollAreaWidgetContents);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_2->addWidget(label_9, 8, 1, 1, 1);

        label_45 = new YesNoLabel(scrollAreaWidgetContents);
        label_45->setObjectName(QStringLiteral("label_45"));

        gridLayout_2->addWidget(label_45, 9, 5, 1, 1);

        pushButton_32 = new TakenButton(scrollAreaWidgetContents);
        pushButton_32->setObjectName(QStringLiteral("pushButton_32"));

        gridLayout_2->addWidget(pushButton_32, 11, 2, 1, 1);

        pushButton_8 = new TakenButton(scrollAreaWidgetContents);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));

        gridLayout_2->addWidget(pushButton_8, 8, 0, 1, 1);

        label_29 = new YesNoLabel(scrollAreaWidgetContents);
        label_29->setObjectName(QStringLiteral("label_29"));

        gridLayout_2->addWidget(label_29, 11, 5, 1, 1);

        pushButton_43 = new TakenButton(scrollAreaWidgetContents);
        pushButton_43->setObjectName(QStringLiteral("pushButton_43"));

        gridLayout_2->addWidget(pushButton_43, 11, 4, 1, 1);

        label_3 = new YesNoLabel(scrollAreaWidgetContents);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 2, 1, 1, 1);

        label_19 = new YesNoLabel(scrollAreaWidgetContents);
        label_19->setObjectName(QStringLiteral("label_19"));

        gridLayout_2->addWidget(label_19, 1, 5, 1, 1);

        label_4 = new YesNoLabel(scrollAreaWidgetContents);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_2->addWidget(label_4, 3, 1, 1, 1);

        label_16 = new YesNoLabel(scrollAreaWidgetContents);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_2->addWidget(label_16, 2, 3, 1, 1);

        pushButton_2 = new TakenButton(scrollAreaWidgetContents);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        gridLayout_2->addWidget(pushButton_2, 2, 0, 1, 1);

        label_12 = new YesNoLabel(scrollAreaWidgetContents);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_2->addWidget(label_12, 3, 3, 1, 1);

        pushButton = new TakenButton(scrollAreaWidgetContents);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        QFont font;
        font.setPointSize(8);
        pushButton->setFont(font);

        gridLayout_2->addWidget(pushButton, 1, 0, 1, 1);

        label_2 = new QLabel(scrollAreaWidgetContents);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font1;
        font1.setPointSize(16);
        label_2->setFont(font1);

        gridLayout_2->addWidget(label_2, 0, 2, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 861, 21));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);
        QObject::connect(pushButton, SIGNAL(clicked(bool)), label, SLOT(toggle(bool)));
        QObject::connect(pushButton_2, SIGNAL(clicked(bool)), label_3, SLOT(toggle(bool)));
        QObject::connect(pushButton_3, SIGNAL(clicked(bool)), label_4, SLOT(toggle(bool)));
        QObject::connect(pushButton_4, SIGNAL(clicked(bool)), label_5, SLOT(toggle(bool)));
        QObject::connect(pushButton_5, SIGNAL(clicked(bool)), label_6, SLOT(toggle(bool)));
        QObject::connect(pushButton_7, SIGNAL(clicked(bool)), label_10, SLOT(toggle(bool)));
        QObject::connect(pushButton_6, SIGNAL(clicked(bool)), label_8, SLOT(toggle(bool)));
        QObject::connect(pushButton_8, SIGNAL(clicked(bool)), label_9, SLOT(toggle(bool)));
        QObject::connect(pushButton_42, SIGNAL(clicked(bool)), label_31, SLOT(toggle(bool)));
        QObject::connect(pushButton_29, SIGNAL(clicked(bool)), label_39, SLOT(toggle(bool)));
        QObject::connect(pushButton_39, SIGNAL(clicked(bool)), label_47, SLOT(toggle(bool)));
        QObject::connect(pushButton_11, SIGNAL(clicked(bool)), label_11, SLOT(toggle(bool)));
        QObject::connect(pushButton_10, SIGNAL(clicked(bool)), label_16, SLOT(toggle(bool)));
        QObject::connect(pushButton_9, SIGNAL(clicked(bool)), label_12, SLOT(toggle(bool)));
        QObject::connect(pushButton_14, SIGNAL(clicked(bool)), label_13, SLOT(toggle(bool)));
        QObject::connect(pushButton_15, SIGNAL(clicked(bool)), label_7, SLOT(toggle(bool)));
        QObject::connect(pushButton_16, SIGNAL(clicked(bool)), label_15, SLOT(toggle(bool)));
        QObject::connect(pushButton_12, SIGNAL(clicked(bool)), label_14, SLOT(toggle(bool)));
        QObject::connect(pushButton_13, SIGNAL(clicked(bool)), label_17, SLOT(toggle(bool)));
        QObject::connect(pushButton_26, SIGNAL(clicked(bool)), label_38, SLOT(toggle(bool)));
        QObject::connect(pushButton_47, SIGNAL(clicked(bool)), label_33, SLOT(toggle(bool)));
        QObject::connect(pushButton_32, SIGNAL(clicked(bool)), label_48, SLOT(toggle(bool)));
        QObject::connect(pushButton_19, SIGNAL(clicked(bool)), label_19, SLOT(toggle(bool)));
        QObject::connect(pushButton_18, SIGNAL(clicked(bool)), label_24, SLOT(toggle(bool)));
        QObject::connect(pushButton_17, SIGNAL(clicked(bool)), label_20, SLOT(toggle(bool)));
        QObject::connect(pushButton_22, SIGNAL(clicked(bool)), label_21, SLOT(toggle(bool)));
        QObject::connect(pushButton_23, SIGNAL(clicked(bool)), label_18, SLOT(toggle(bool)));
        QObject::connect(pushButton_24, SIGNAL(clicked(bool)), label_23, SLOT(toggle(bool)));
        QObject::connect(pushButton_20, SIGNAL(clicked(bool)), label_22, SLOT(toggle(bool)));
        QObject::connect(pushButton_21, SIGNAL(clicked(bool)), label_25, SLOT(toggle(bool)));
        QObject::connect(pushButton_31, SIGNAL(clicked(bool)), label_45, SLOT(toggle(bool)));
        QObject::connect(pushButton_41, SIGNAL(clicked(bool)), label_30, SLOT(toggle(bool)));
        QObject::connect(pushButton_43, SIGNAL(clicked(bool)), label_29, SLOT(toggle(bool)));
        QObject::connect(pushButton, SIGNAL(clicked(bool)), pushButton, SLOT(sendTakenSignal()));
        QObject::connect(pushButton, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_11, SIGNAL(clicked(bool)), pushButton_11, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_11, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_19, SIGNAL(clicked(bool)), pushButton_19, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_19, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_2, SIGNAL(clicked(bool)), pushButton_2, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_10, SIGNAL(clicked(bool)), pushButton_10, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_18, SIGNAL(clicked(bool)), pushButton_18, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_3, SIGNAL(clicked(bool)), pushButton_3, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_9, SIGNAL(clicked(bool)), pushButton_9, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_17, SIGNAL(clicked(bool)), pushButton_17, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_4, SIGNAL(clicked(bool)), pushButton_4, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_14, SIGNAL(clicked(bool)), pushButton_14, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_22, SIGNAL(clicked(bool)), pushButton_22, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_5, SIGNAL(clicked(bool)), pushButton_5, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_15, SIGNAL(clicked(bool)), pushButton_15, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_23, SIGNAL(clicked(bool)), pushButton_23, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_7, SIGNAL(clicked(bool)), pushButton_7, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_16, SIGNAL(clicked(bool)), pushButton_16, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_24, SIGNAL(clicked(bool)), pushButton_24, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_6, SIGNAL(clicked(bool)), pushButton_6, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_12, SIGNAL(clicked(bool)), pushButton_12, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_20, SIGNAL(clicked(bool)), pushButton_20, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_8, SIGNAL(clicked(bool)), pushButton_8, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_13, SIGNAL(clicked(bool)), pushButton_13, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_21, SIGNAL(clicked(bool)), pushButton_21, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_42, SIGNAL(clicked(bool)), pushButton_42, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_26, SIGNAL(clicked(bool)), pushButton_26, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_31, SIGNAL(clicked(bool)), pushButton_31, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_29, SIGNAL(clicked(bool)), pushButton_29, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_47, SIGNAL(clicked(bool)), pushButton_47, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_41, SIGNAL(clicked(bool)), pushButton_41, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_39, SIGNAL(clicked(bool)), pushButton_39, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_32, SIGNAL(clicked(bool)), pushButton_32, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_43, SIGNAL(clicked(bool)), pushButton_43, SLOT(sendTakenSignal()));
        QObject::connect(pushButton_2, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_10, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_18, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_3, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_9, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_17, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_4, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_14, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_22, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_5, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_15, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_23, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_7, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_16, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_24, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_6, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_12, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_20, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_13, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_21, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_31, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_26, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_8, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_42, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_29, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_41, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_39, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_47, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_32, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(pushButton_43, SIGNAL(takenSignal(QString)), MainWindow, SLOT(passTakenToStatusButton(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_2, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_3, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_4, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_5, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_7, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_6, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_8, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_11, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_42, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_10, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_9, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_14, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_15, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_16, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_12, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_13, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_26, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_19, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_18, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_17, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_22, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_23, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_24, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_20, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_21, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_31, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_29, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_47, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_41, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_39, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_32, SLOT(noteTaken(QString)));
        QObject::connect(MainWindow, SIGNAL(announceTaken(QString)), pushButton_43, SLOT(noteTaken(QString)));
        QObject::connect(pushButton, SIGNAL(makeChecked()), label, SLOT(check()));
        QObject::connect(pushButton_11, SIGNAL(makeChecked()), label_11, SLOT(check()));
        QObject::connect(pushButton_19, SIGNAL(makeChecked()), label_19, SLOT(check()));
        QObject::connect(pushButton_2, SIGNAL(makeChecked()), label_3, SLOT(check()));
        QObject::connect(pushButton_10, SIGNAL(makeChecked()), label_16, SLOT(check()));
        QObject::connect(pushButton_18, SIGNAL(makeChecked()), label_24, SLOT(check()));
        QObject::connect(pushButton_3, SIGNAL(makeChecked()), label_4, SLOT(check()));
        QObject::connect(pushButton_9, SIGNAL(makeChecked()), label_12, SLOT(check()));
        QObject::connect(pushButton_17, SIGNAL(makeChecked()), label_20, SLOT(check()));
        QObject::connect(pushButton_4, SIGNAL(makeChecked()), label_5, SLOT(check()));
        QObject::connect(pushButton_14, SIGNAL(makeChecked()), label_13, SLOT(check()));
        QObject::connect(pushButton_22, SIGNAL(makeChecked()), label_21, SLOT(check()));
        QObject::connect(pushButton_5, SIGNAL(makeChecked()), label_6, SLOT(check()));
        QObject::connect(pushButton_15, SIGNAL(makeChecked()), label_7, SLOT(check()));
        QObject::connect(pushButton_23, SIGNAL(makeChecked()), label_18, SLOT(check()));
        QObject::connect(pushButton_7, SIGNAL(makeChecked()), label_10, SLOT(check()));
        QObject::connect(pushButton_16, SIGNAL(makeChecked()), label_15, SLOT(check()));
        QObject::connect(pushButton_24, SIGNAL(makeChecked()), label_23, SLOT(check()));
        QObject::connect(pushButton_6, SIGNAL(makeChecked()), label_8, SLOT(check()));
        QObject::connect(pushButton_12, SIGNAL(makeChecked()), label_14, SLOT(check()));
        QObject::connect(pushButton_20, SIGNAL(makeChecked()), label_22, SLOT(check()));
        QObject::connect(pushButton_8, SIGNAL(makeChecked()), label_9, SLOT(check()));
        QObject::connect(pushButton_13, SIGNAL(makeChecked()), label_17, SLOT(check()));
        QObject::connect(pushButton_21, SIGNAL(makeChecked()), label_25, SLOT(check()));
        QObject::connect(pushButton_42, SIGNAL(makeChecked()), label_31, SLOT(check()));
        QObject::connect(pushButton_26, SIGNAL(makeChecked()), label_38, SLOT(check()));
        QObject::connect(pushButton_31, SIGNAL(makeChecked()), label_45, SLOT(check()));
        QObject::connect(pushButton_29, SIGNAL(makeChecked()), label_39, SLOT(check()));
        QObject::connect(pushButton_47, SIGNAL(makeChecked()), label_33, SLOT(check()));
        QObject::connect(pushButton_41, SIGNAL(makeChecked()), label_30, SLOT(check()));
        QObject::connect(pushButton_39, SIGNAL(makeChecked()), label_47, SLOT(check()));
        QObject::connect(pushButton_32, SIGNAL(makeChecked()), label_48, SLOT(check()));
        QObject::connect(pushButton_43, SIGNAL(makeChecked()), label_29, SLOT(check()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label_15->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_7->setText(QApplication::translate("MainWindow", "HUMA 201", 0));
        pushButton_24->setText(QApplication::translate("MainWindow", "COMP 252", 0));
        pushButton_21->setText(QApplication::translate("MainWindow", "COMP 361", 0));
        pushButton_47->setText(QApplication::translate("MainWindow", "COMP 452", 0));
        pushButton_26->setText(QApplication::translate("MainWindow", "HUMA 301", 0));
        pushButton_19->setText(QApplication::translate("MainWindow", "PHYS 101", 0));
        label_17->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_33->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_39->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_12->setText(QApplication::translate("MainWindow", "COMP 314", 0));
        pushButton_13->setText(QApplication::translate("MainWindow", "COMP 342", 0));
        pushButton_31->setText(QApplication::translate("MainWindow", "COMP 422", 0));
        pushButton_20->setText(QApplication::translate("MainWindow", "COMP 322", 0));
        label_21->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_16->setText(QApplication::translate("MainWindow", "HUMA 202", 0));
        label_22->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_22->setText(QApplication::translate("MainWindow", "COMP 222", 0));
        label_25->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_23->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_15->setText(QApplication::translate("MainWindow", "COMP 244", 0));
        pushButton_23->setText(QApplication::translate("MainWindow", "CHEM 105", 0));
        label_6->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_29->setText(QApplication::translate("MainWindow", "COMP 451", 0));
        pushButton_5->setText(QApplication::translate("MainWindow", "MATH 213", 0));
        pushButton_14->setText(QApplication::translate("MainWindow", "COMP 220", 0));
        label_7->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_18->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_13->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_5->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_10->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_6->setText(QApplication::translate("MainWindow", "COMP 305", 0));
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_11->setText(QApplication::translate("MainWindow", "MATH 162", 0));
        label_20->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_24->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_17->setText(QApplication::translate("MainWindow", "MATH 261", 0));
        pushButton_18->setText(QApplication::translate("MainWindow", "COMP 141", 0));
        pushButton_4->setText(QApplication::translate("MainWindow", "MATH 214", 0));
        pushButton_10->setText(QApplication::translate("MainWindow", "COMP 155", 0));
        pushButton_9->setText(QApplication::translate("MainWindow", "HUMA 102", 0));
        label_11->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_41->setText(QApplication::translate("MainWindow", "COMP 442", 0));
        label_47->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_30->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_38->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_14->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_42->setText(QApplication::translate("MainWindow", "COMP 450", 0));
        pushButton_39->setText(QApplication::translate("MainWindow", "COMP 443", 0));
        pushButton_3->setText(QApplication::translate("MainWindow", "HUMA 101", 0));
        label_31->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_48->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_8->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_9->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_45->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_32->setText(QApplication::translate("MainWindow", "COMP 448", 0));
        pushButton_8->setText(QApplication::translate("MainWindow", "COMP 340", 0));
        label_29->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_43->setText(QApplication::translate("MainWindow", "HUMA 302", 0));
        label_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_19->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_4->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        label_16->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "PHYS 102", 0));
        label_12->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/team_awesome_project/GeneratedFiles/Debug/no.jpeg\"/></p></body></html>", 0));
        pushButton->setText(QApplication::translate("MainWindow", "MATH 161", 0));
        label_2->setText(QApplication::translate("MainWindow", "Status Sheet", 0));
    } // retranslateUi

};

namespace UiStat {
    class statusClass: public ui_statusClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STATUS_H
