#include <QObject>
#include <QPushButton> 
#include <string>
#include <vector>
#include "AvalibleClasses.h"
#pragma once
class CalendarSlot:public QPushButton
{
	Q_OBJECT
public:
	CalendarSlot(QWidget* parent = nullptr): QPushButton(parent) {
	};
public slots:
	void changeText(QString);
	void deleteCourse(bool);
	void recieveAvailable(AvalibleClasses a){avalible = new AvalibleClasses(a);}//Set the available courses
signals:
private:
	AvalibleClasses* avalible;//The available courses
	std::vector<std::string> conflictingCourses;//In case the user tries to add more than one course at the same time.
	QString getCourseInfo(QString);//to search and get info for the course on the button
};