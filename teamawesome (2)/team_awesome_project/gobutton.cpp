#include "gobutton.h"
#include <fstream>
void GoButton::courseChanged()
{
	requestCourseName();
}
void GoButton::recieveCourseName(QString name)
{
	courseName=name.toStdString();
}
void GoButton::dateTimeChanged(QString time)
{
	dateTime = time.toStdString();
}
void GoButton::gotPushed(bool)
{
	searchResults.clear();//Empty the search results from last search
	fstream file;
	file.open("CourseDB_WithFictionalCapacities (1).txt");
	fstream prereqs;//prerequisites
	prereqs.open("prerequisites.txt");
	AvalibleClasses* available = new AvalibleClasses(file,prereqs);
	searchResults=available->courseSearch(courseName,dateTime);
	if(searchResults.size()<1)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult1(QString(((searchResults[0]).code).c_str()));
	if(searchResults.size()<2)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult2(QString(((searchResults[1]).code).c_str()));
	if(searchResults.size()<3)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult3(QString(((searchResults[2]).code).c_str()));
	if(searchResults.size()<4)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult4(QString(((searchResults[3]).code).c_str()));
	if(searchResults.size()<5)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult5(QString(((searchResults[4]).code).c_str()));
	if(searchResults.size()<6)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult6(QString(((searchResults[5]).code).c_str()));
	if(searchResults.size()<7)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult7(QString(((searchResults[6]).code).c_str()));
	if(searchResults.size()<8)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult8(QString(((searchResults[7]).code).c_str()));
	if(searchResults.size()<9)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult9(QString(((searchResults[8]).code).c_str()));
	if(searchResults.size()<10)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult10(QString(((searchResults[9]).code).c_str()));
	if(searchResults.size()<11)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult11(QString(((searchResults[10]).code).c_str()));
	if(searchResults.size()<12)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult12(QString(((searchResults[11]).code).c_str()));
	if(searchResults.size()<13)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult13(QString(((searchResults[12]).code).c_str()));
	if(searchResults.size()<14)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult14(QString(((searchResults[13]).code).c_str()));
	if(searchResults.size()<15)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult15(QString(((searchResults[14]).code).c_str()));
	if(searchResults.size()<16)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult16(QString(((searchResults[15]).code).c_str()));
	if(searchResults.size()<17)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult17(QString(((searchResults[16]).code).c_str()));
	if(searchResults.size()<18)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult18(QString(((searchResults[17]).code).c_str()));
	if(searchResults.size()<19)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult19(QString(((searchResults[18]).code).c_str()));
	if(searchResults.size()<20)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult20(QString(((searchResults[19]).code).c_str()));
	if(searchResults.size()<21)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult21(QString(((searchResults[20]).code).c_str()));
	if(searchResults.size()<22)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult22(QString(((searchResults[21]).code).c_str()));
	if(searchResults.size()<23)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult23(QString(((searchResults[22]).code).c_str()));
	if(searchResults.size()<24)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult24(QString(((searchResults[23]).code).c_str()));
	if(searchResults.size()<25)//Don't try to display more results then there are and read off the end of the array
		return;
	changeResult25(QString(((searchResults[24]).code).c_str()));
}
void GoButton::courseAdded(int resultNum)
{
	if(resultNum>=searchResults.size())
		return;
	if((searchResults.at(resultNum)).day=="MWF" && (searchResults.at(resultNum)).time=="8:00:00")
		addClass8mwf(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="MWF" && (searchResults.at(resultNum)).time=="9:00:00")
		addClass9mwf(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="MWF" && (searchResults.at(resultNum)).time=="10:00:00")
		addClass10mwf(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="MWF" && (searchResults.at(resultNum)).time=="11:00:00")
		addClass11mwf(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="MWF" && (searchResults.at(resultNum)).time=="12:00:00")
		addClass12mwf(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="MWF" && (searchResults.at(resultNum)).time=="13:00:00")
		addClass1mwf(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="MWF" && (searchResults.at(resultNum)).time=="14:00:00")
		addClass2mwf(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="MWF" && (searchResults.at(resultNum)).time=="15:00:00")
		addClass3mwf(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="TR" && (searchResults.at(resultNum)).time=="8:00:00")
		addClass8tr(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="TR" && (searchResults.at(resultNum)).time=="10:05:00")
		addClass10tr(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="TR" && (searchResults.at(resultNum)).time=="11:30:00")
		addClass11tr(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="TR" && (searchResults.at(resultNum)).time=="13:00:00")
		addClass1tr(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="TR" && (searchResults.at(resultNum)).time=="14:30:00")
		addClass2tr(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="M")
		addClass6m(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="T")
		addClass6t(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="W")
		addClass6w(QString(((searchResults[resultNum]).code).c_str()));
	else if((searchResults.at(resultNum)).day=="R")
		addClass6r(QString(((searchResults[resultNum]).code).c_str()));
	else
		addClassInd(QString(((searchResults[resultNum]).code).c_str()));
	
}
void GoButton::markAsTaken(QString courseCode)
{
	/*
	fstream file;
	file.open("CourseDB_WithFictionalCapacities (1).txt");
	fstream prereqs;//prerequisites
	prereqs.open("prerequisites.txt");
	AvalibleClasses* available = new AvalibleClasses(file,prereqs);
		
	if(available->checkCanTake( std::string(courseCode.toUtf8()) ) )
		available->markAsTaken( std::string(courseCode.toUtf8()) );
	else
		available->unMarkAsTaken( std::string(courseCode.toUtf8()) );
		*/
}