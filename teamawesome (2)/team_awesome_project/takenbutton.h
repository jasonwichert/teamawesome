#include <QObject>
#include <QPushButton> 
#pragma once
class TakenButton:public QPushButton
{
	Q_OBJECT
public:
	TakenButton(QWidget* parent = nullptr): QPushButton(parent) {};
public slots:
	void sendTakenSignal();//When the button is pushed, we send a signal to the status window.
	void noteTaken(QString code){if(code[0].toUpper()==text()[0]&&code[1].toUpper()==text()[1]&&code[2].toUpper()==text()[2]&&code[3].toUpper()==text()[3]&&code[5].toUpper()==text()[5]&&code[6].toUpper()==text()[6]&&code[7].toUpper()==text()[7]){makeChecked();}}//The taken button notes that a course has been added, if it this course, it sends a signal to the yesnolabel to change its status
signals:
	void takenSignal(QString);//The signal that we send to the status window.
	void makeChecked();//Tell the label that this course has been taken.
private:
};

