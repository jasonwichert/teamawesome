#include <QObject>
#include <QPushButton> 
#include <string>
#include <vector>
#include "AvalibleClasses.h"
#pragma once
class PreReqsButton:public QPushButton
{
	Q_OBJECT
public:
	PreReqsButton(QWidget* parent = nullptr): QPushButton(parent) {};
public slots:
	void changeToolTip(QString t);
	void recieveAvailable(AvalibleClasses a){avalible = new AvalibleClasses(a);}//Set the available courses
signals:
	
private:
	AvalibleClasses* avalible;//Available courses
	//to search and prereqs for the course on the button
	QString getPreReqs(QString t);
};