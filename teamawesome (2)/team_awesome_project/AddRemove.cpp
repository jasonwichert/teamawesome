#include "AddRemove.h"
//
AddRemove::AddRemove()
{
	//since there are 34 different possible date/time slots available, intialize each to "empty"
	for(int i = 0; i < 34; i++)
	{
		courseNames.push_back("empty");
	}
}

void AddRemove::addCourse(std::string course, int dateTimeParam)
{
	//dateTimeParam is a # btwn 0 and 33, each number corresponding to a time slot
	for (int i = 0; i < courseNames.size(); i++)
	{
		//check to make sure course isn't already added
		if (course != courseNames[i])
		{
			//check to ensure that slot is available
			if (courseNames[dateTimeParam] == "empty")
			{
				courseNames[dateTimeParam] = course;
			}
		}
	}
}

void AddRemove::removeCourse(std::string course)
{
	for (int i = 0; i < courseNames.size(); i++)
	{
		//find the name of the course in the courseNames vector and makes the value "empty" course is found
		if (courseNames[i] == course)
		{
			courseNames[i] = "empty";
		}
	}
}