#include <QObject>
#include <QPushButton> 
#include <string>
#include <vector>
#include "AvalibleClasses.h"
#pragma once
class ResultButton:public QPushButton
{
	Q_OBJECT
public:
	ResultButton(QWidget* parent = nullptr): QPushButton(parent) {};
public slots:
	void ChangeText(QString t);
	void recieveAvailable(AvalibleClasses a){avalible = new AvalibleClasses(a);}//Set the available courses
signals:
	void declareTextChanged(QString);//Tell the prerequisites button that what the class associated with it is now
private:
	//to search and get info for the course on the button
	AvalibleClasses* avalible;
	QString getCourseInfo(QString);
};