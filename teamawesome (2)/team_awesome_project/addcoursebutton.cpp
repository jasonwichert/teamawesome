#include "addcoursebutton.h"

void AddCourseButton::buttonPushed(bool)
{
	QByteArray ba = objectName().toLatin1();
	const char *c_str = ba.data(); 
	indicatePushed(atoi(&c_str[1]));
}