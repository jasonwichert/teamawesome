#include "prereqsbutton.h"

void PreReqsButton::changeToolTip(QString t)
{
	setToolTip(getPreReqs(t));  //change button text to prerequisites
}

QString PreReqsButton::getPreReqs(QString t){
	vector<AvalibleClasses::Classes> results = avalible->searchCode(t.toStdString(),avalible->getList());
	string moreInfo = " ";
	for(int i = 0; i < results.at(0).prerequisite.size(); i++)
	{
		moreInfo += results.at(0).prerequisite.at(i) + ", ";
	}
	return QString(moreInfo.c_str());
}