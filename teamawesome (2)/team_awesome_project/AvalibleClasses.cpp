#include"AvalibleClasses.h"
#include<iostream>
#include<string>
AvalibleClasses::AvalibleClasses(){

}

//Takes the given file and stores all the information from one line in 
AvalibleClasses::AvalibleClasses(fstream& file, fstream& prerequisite){
	while(!file.eof()){
		Classes temp;
		file >>  temp.code;
		file >> temp.shortTitle;
		file >> temp.name;
		file >> temp.time;
		file >> temp.endTime;
		file >> temp.day;
		file >> temp.building;
		file >> temp.room;
		file >> temp.enrollment;
		file >> temp.capacity;
		while(!prerequisite.eof()){
			string tempCode, tempPre;
			prerequisite >> tempCode;
			prerequisite >> tempPre;
			if(temp.code.find(tempCode) != string::npos){
				temp.prerequisite.push_back(tempPre);
			}
		}
		prerequisite.seekg(0, prerequisite.beg);
		temp.hasTaken = false;
		temp.canTake = false;
		list.push_back(temp);
	}
}


//Seaches each course code for the given string
//Returns a vector containing the courses whoes course codes contain the string
vector<AvalibleClasses::Classes> AvalibleClasses::searchCode(string input, vector<AvalibleClasses::Classes> classes){
	vector<AvalibleClasses::Classes> result;
	for(int i = 0; i < classes.size(); i++){
		if(classes.at(i).code.find(input) != string::npos){
			result.push_back(classes.at(i));
		}
	}
	return result;
}

//Seaches each course Name for the given string
//Returns a vector containing the courses whoes course name contain the string
vector<AvalibleClasses::Classes> AvalibleClasses::searchName(string input, vector<AvalibleClasses::Classes> classes){
	vector<AvalibleClasses::Classes> result;
	for(int i = 0; i < classes.size(); i++){
		if(classes.at(i).name.find(input) != string::npos){
			result.push_back(classes.at(i));
		}
	}
	return result;
}

//Seaches each course time for the given string
//Returns a vector containing the courses whoes course time contain the string
vector<AvalibleClasses::Classes> AvalibleClasses::searchTime(string input, vector<AvalibleClasses::Classes> classes){
	vector<AvalibleClasses::Classes> result;
	for(int i = 0; i < classes.size(); i++){
		if(classes.at(i).time.find(input) != string::npos){
			result.push_back(classes.at(i));
		}
	}
	return result;
}

//Seaches each course Day for the given string
//Returns a vector containing the courses whoes course Day contain the string
vector<AvalibleClasses::Classes> AvalibleClasses::searchDay(string input, vector<AvalibleClasses::Classes> classes){
	vector<AvalibleClasses::Classes> result;
	for(int i = 0; i < classes.size(); i++){
		if(classes.at(i).day.find(input) != string::npos){
			result.push_back(classes.at(i));
		}
	}
	return result;
}


//takes in a time, day, and string and returns a list of courses that take place at that time, on that day,
//and have that string in their course name or course code
vector<AvalibleClasses::Classes> AvalibleClasses::courseSearch(string courseName, string dateTime)
{
	//start with the full list of courses

	vector<AvalibleClasses::Classes> result = AvalibleClasses::list;
	//make the string useable
	courseName = convert(courseName);
	//if you're searching by a time
	if(dateTime!="Any Time"&&dateTime!="")
	{
		//make the dateTime string usable
		vector<string> parsed = parseDateTime(dateTime);
		//eliminate all courses not at that time
		result = searchTime(parsed.at(0), result);
		//eliminate all courses not at that day
		result = searchDay(parsed.at(1), result);
	}
	//copy this list
	vector<AvalibleClasses::Classes> byName = result;
	if(courseName != ""){
		//search identical lists by course name and course code
		result = searchCode(courseName, result);
		byName = searchName(courseName, byName);
		//add the unique elements in byName to results
		result = merge(result, byName);
	}
	return result;	
}

//converts the input string to a useable form
//makes the string all capital letters and replaces spaces with underscores
string AvalibleClasses::convert(string input){
	int i = 0;
	char c;
	while (input[i]){
		c=input[i];
		input[i] = toupper(c);
		i++;
	}
	while(input.find(" ") != string::npos){
		input.replace(input.find(" "), 1, "_");
	}
	return input;
}


//breaks down dateTime into time and days
vector<string> AvalibleClasses::parseDateTime(string dateTime){
	vector<string> result;
	string temp;
	//This loop gets the hour part of the time
	for(int i= 0; i < dateTime.find(":"); i ++){
		temp += dateTime.at(i);
	}
	//if the hour is less than eight (the class is in the afternoon)
	//convert it to military time
	if(stoi(temp) < 8){
		int number;
		number = stoi(temp) + 12;
		temp = to_string(number);
	}
	//add the minutes and seconds back to the time
	temp += dateTime.substr(dateTime.find(":"), 3);
	result.push_back(temp);

	//each date time is of the form (00:00:00 AM/PM DAYS)
	//need to get rid of the time then the AM/PM and leave DAYS
	int position;
	//get rid of the time
	position = dateTime.find(" ");
	temp = dateTime.substr(position+1);
	//get rid of the AM/PM
	position = temp.find(" ");
	temp = temp.substr(position+1);
	result.push_back(temp);
	return result;
}

//Combines two vectors into one eliminating the redundant elements
vector<AvalibleClasses::Classes> AvalibleClasses::merge(vector<AvalibleClasses::Classes> base, vector<AvalibleClasses::Classes> additional){
	vector<AvalibleClasses::Classes> temp;
	for(int i = 0; i < additional.size(); i++){
		//search for each additional course in base
		temp = searchCode(additional.at(i).code, base);
		//if the additional course is not in base add it to base
		if(temp.size() == 0){
			base.push_back(additional.at(i));
		}
	}
	return base;
}

//Checks each prerequisite, and if one of them hasn't been taken it returns false
//if they have all been taken or it has no prerequisites it returns true
bool AvalibleClasses::checkCanTake(string check){
	vector<AvalibleClasses::Classes> testClass = searchCode(check, AvalibleClasses::list);
	for(int i = 0; i < testClass.at(0).prerequisite.size(); i++){
		vector<AvalibleClasses::Classes> result = searchCode(testClass.at(0).prerequisite.at(i), AvalibleClasses::list);
		if(result.size() > 0){
			if(result.at(0).hasTaken == false){
				return false;
			}
		}
	}
	return true;
}

//this is called when a class is maked as taken
//it changes hasTaken to true for each section of the class
void AvalibleClasses::markAsTaken(string taken){
	vector<AvalibleClasses::Classes> markedClasses = searchCode(taken, list);
	int marker = 0;
	for(int i = 0; i < list.size(); i++){
		if(list.at(i).code.find(taken) != string::npos){
			marker = i;
			i = list.size();
		}
	}
	for(int j = marker; j < (marker + markedClasses.size()); j++){
		list.at(j).hasTaken = true;
	}
}
void AvalibleClasses::unMarkAsTaken(string taken){
	vector<AvalibleClasses::Classes> markedClasses = searchCode(taken, list);
	int marker = 0;
	for(int i = 0; i < list.size(); i++){
		if(list.at(i).code.find(taken) != string::npos){
			marker = i;
			i = list.size();
		}
	}
	for(int j = marker; j < (marker + markedClasses.size()); j++){
		list.at(j).hasTaken = false;
	}
}

//builds the list of codes and names that is the status sheet
void AvalibleClasses::buildStatusSheet(fstream& statusSheet){
	while(!statusSheet.eof()){
		vector<string> temp;
		string tempStorage;
		statusSheet >> tempStorage;
		temp.push_back(tempStorage);
		statusSheet >> tempStorage;
		temp.push_back(tempStorage);
		statusSheetClasses.push_back(temp);
	}
}