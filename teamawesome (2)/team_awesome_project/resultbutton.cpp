#include "resultbutton.h"

void ResultButton::ChangeText(QString t)
{
		setText(t);
		setToolTip(getCourseInfo(t));
		declareTextChanged(t);
}

QString ResultButton::getCourseInfo(QString code)
{
	//make database, search it, find course and get info, concatenate into one string
	
	vector<AvalibleClasses::Classes> results = avalible->searchCode(code.toStdString(),avalible->getList());
	string moreInfo = "Time: " + results.at(0).time + " - " + results.at(0).endTime + "; " + "Location: " + results.at(0).building + " " + results.at(0).room;
	return QString(moreInfo.c_str());
}