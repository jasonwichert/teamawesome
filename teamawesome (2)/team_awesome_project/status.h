#ifndef STATUS_H
#define STATUS_H
#include <QtWidgets/QMainWindow>
#include "uiStatus.h"
#include "AvalibleClasses.h"
#include "qpushbutton.h"
class status : public QMainWindow
{
	Q_OBJECT

public:
	status(QWidget *parent = 0);
	~status();
	void setStatusSheetButtonPntr(QPushButton* pntr){statusSheetButtonPntr=pntr;}//Set statusSheetButtonPntr
public slots:
	void passTakenToStatusButton(QString courseCode){statusSheetButtonPntr->windowIconTextChanged(courseCode);}//Send the course code to the status sheet button, and then it will go to the go button.
signals:
	void announceTaken(QString);//Let all of the courses buttons know that a class was added so they can check if it is them.
private:
	UiStat::statusClass ui;
	QPushButton* statusSheetButtonPntr;//This is so the status sheet window can communicate with the main window
};

#endif 
