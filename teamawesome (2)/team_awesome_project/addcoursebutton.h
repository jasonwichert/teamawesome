#include <QObject>
#include <QPushButton> 
#include <string>
#include <vector>
#include "AvalibleClasses.h"
#pragma once
class AddCourseButton:public QPushButton
{
	Q_OBJECT
public:
	AddCourseButton(QWidget* parent = nullptr): QPushButton(parent) {};
public slots:
	void buttonPushed(bool);
signals:
	void indicatePushed(int);
private:
};