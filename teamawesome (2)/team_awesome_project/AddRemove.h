#include <QObject>
#include <QPushButton> 
#include <string>
#include <vector>
#pragma once
class AddRemove:public QPushButton
{
public:
	//default constructor initializing course names to "empty" for all 34 slots
	AddRemove();

	//adds course into a vector based on the dateTimeParam
	void addCourse(std::string course, int dateTimeParam);

	//removes course given a course name
	void removeCourse(std::string course);

private:
	//vector for storing the course names in time slots
	std::vector<std::string> courseNames;
};